import keywordExtractor from "keyword-extractor";
import LanguageLayerAPI from "language-detection";
import fs from "fs";

const languageLayerAPI = new LanguageLayerAPI({
  access_key: "34f8f69185bb768ceabe4742972a4503"
});
const whitelistedKeyWords = {
  composite: "composite",
  direct: "direct",
  restoration: "restoration",
  treatment: "treatment",
  upper: "upper",
  bite: "bite",
  smile: "smile",
  crown: "crown",
  posterior: "posterior",
  lower: "lower",
  crowns: "crowns",
  canal: "canal",
  emax: "emax",
  root: "root",
  bone: "bone",
  full: "full",
  anterior: "anterior",
  tissue: "tissue",
  rct: "rct",
  matrix: "matrix",
  molar: "molar",
  implants: "implants",
  post: "post",
  restorations: "restorations",
  isolation: "isolation",
  ceramic: "ceramic",
  caries: "caries",
  endo: "endo",
  enamel: "enamel",
  shade: "shade",
  premolar: "premolar",
  extraction: "extraction",
  soft: "soft",
  cavity: "cavity",
  central: "central",
  lateral: "lateral",
  system: "system",
  gingival: "gingival",
  endodontic: "endodontic",
  veneer: "veneer",
  placement: "placement",
  apical: "apical",
  natural: "natural",
  female: "female",
  bridge: "bridge",
  distal: "distal",
  canine: "canine",
  filling: "filling",
  anatomy: "anatomy",
  pulp: "pulp",
  surgery: "surgery",
  broken: "broken",
  gc: "gc",
  resin: "resin",
  con: "con",
  pain: "pain",
  "e-max": "e-max",
  graft: "graft",
  gingivectomy: "gingivectomy",
  implant: "implants",
  veneers: "veneer",
  canals: "canal"
};

//utilities
export const aggregator = (acc, curr) =>
  acc[curr] ? { ...acc, [curr]: acc[curr] + 1 } : { ...acc, [curr]: 1 };
export const flatten = (acc, curr) => [...acc, ...curr];
export const sum = (acc, curr)=> acc + curr;
export const objectToArray = object =>
  Object.keys(object).map(k => ({ keyWord: k, count: object[k] }));
export const getDuplicates = (arr1, arr2) => {
  const duplicates = {};
  arr1.forEach(word => {
    if (arr2.indexOf(word) !== -1) {
      duplicates[word] = 1;
    }
  });
  return Object.keys(duplicates);
};

const removeDuplicates = arrArg =>
  arrArg.filter((elem, pos, arr) => arr.indexOf(elem) === pos);

export const exportJSON = outputFileName => data =>
  fs.writeFile(outputFileName, JSON.stringify(data, null, "\t"), "utf8");

//extract
export const extractKeywordsV1 = string => string.split(" ");
export const extractKeywordsV2 = string =>
  removeDuplicates(keywordExtractor.extract(string));
export const detectLanguages = string =>
  languageLayerAPI
    .detect({ query: string })
    .then(result => result[0].language_name);
export const extendPostWithLanguage = post =>
  new Promise(
    (resolve, reject) =>
      post.language
        ? resolve(post)
        : detectLanguages(post.title + " " + post.description)
            .then(language => resolve({ ...post, language }))
            .catch(err => resolve(post))
  );

export const extendPostWithKeyWords = post => ({
  ...post,
  keywords: extractKeywordsV2(post.title + " " + post.description)
    .map(keyword => whitelistedKeyWords[keyword])
    .filter(x => !!x)
});

// match score
export const getPostKeyWordsMatchScore = (post1, post2) => {
  const duplicates = getDuplicates(post1.keywords, post2.keywords);
  return duplicates.length;
};
