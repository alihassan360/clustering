import posts from "./raw3.json";
import {getPostKeyWordsMatchScore, aggregator, objectToArray, exportJSON, flatten, sum} from "./util.js";


const centeroids = getRandomCenteroids(posts, 40);
getClusters(centeroids)
let lastError = 0;

function getClusters(unfilteredCenteroids) {
    let clusters = [];
    console.log(unfilteredCenteroids.length);
    const centeroids = unfilteredCenteroids.filter(x=> !!x);
    console.log(centeroids.length);
    for (let post of posts) {
        const closestCenteroid = getShortestDistance(post, centeroids);
        if(clusters[closestCenteroid.id]){
          clusters[closestCenteroid.id].push(post)  
        }
        else {
          clusters[closestCenteroid.id] = [post]  
        }
    }

    const newCenteroids = getCenteroids(clusters);
    const error = newCenteroids.map(getCenteroidError).reduce(sum, 0);
    if(error === lastError){
        return exportJSON(`trials/${error}.json`)(newCenteroids);
        
    }
    else {
        lastError = error;
    }
    console.log(error);
    return getClusters(newCenteroids);
}

function getRandomCenteroids(posts, k) {
    const centeroids = [];
    let i = 0;
    while (i < k) {
        const randomPostId = Math.floor(Math.random() * (posts.length));
        if (!containsId(centeroids, randomPostId)) {
          centeroids.push({ post: posts[randomPostId], id: i });
            i++;
        }
    }
    return centeroids;
}

function containsId(centeroids, id) {
    for (let centeroid of centeroids) {
        if (centeroid.id === id) {
            return true;
        }
    }
    return false;
}

function getDistance (post1, post2) {
//   console.log(Object.keys(post1), Object.keys(post2));
  const caseTypeScore = post1.caseType === post2.caseType ? 1: 0;
  const specialityScore = post1.speciality === post2.speciality ? 1: 0;
  const keywordsScore = getPostKeyWordsMatchScore(post1, post2); 
  const languageScore = post1.language === post2.language? 1: 0;
  // console.log(caseTypeScore , specialityScore , keywordsScore , languageScore)
  return caseTypeScore + specialityScore + keywordsScore + languageScore;
}

function getErrorBetweenTwoPosts(post1, post2){
    const caseTypeError = post1.caseType === post2.caseType ? 0: 1;
    const specialityError = post1.speciality === post2.speciality ? 0: 1;
    const keywordsError = post1.keywords.length + post2.keywords.length - getPostKeyWordsMatchScore(post1, post2); 
    const languageError = post1.language === post2.language? 0 : 1;
    return caseTypeError + specialityError + keywordsError + languageError;
}
function getCenteroidError(centeroid) {
    if(!centeroid)
        return 100;//show big error if a cluster is empty
    return centeroid.cluster.map(post=>getErrorBetweenTwoPosts(post, centeroid.post)).reduce(sum, 0);
}


function getShortestDistance(post, centeroids) {
  const distances = centeroids.map(centroid=> getDistance(centroid.post , post));
  const minIndex = distances.indexOf(Math.max(...distances));

  return {id: minIndex};
}

function getCenteroids(clusters) {
    const centeroids = [];
    // console.log(clusters.length, clusters.map(x=> x? x.length: undefined));
    for (let i=0; i<clusters.length; i++) {
        const centeroid = !clusters[i]? undefined :{
            id: i,
            post: {
                caseType: getMostCommonCaseType(clusters[i]||[]),
                speciality: getMostCommonSpeciality(clusters[i]||[]),
                keywords: getMostCommonKeywords(clusters[i]||[]),
                language: getMostCommonLanguage(clusters[i]||[]),
            },
            cluster: clusters[i] 
        }
        centeroids.push(centeroid);
    }
    // console.log(centeroids);
    return centeroids;
}

function getMostCommon(array) {
    return (objectToArray(array.reduce(aggregator, {})).sort((a, b) => b.count - a.count)) || [];
}
function getMostCommonCaseType(cluster) {
    return getMostCommon(cluster.map(x=>x.caseType))[0].keyWord;
}
function getMostCommonLanguage(cluster) {
    return getMostCommon(cluster.map(x=>x.language))[0].keyWord;
}
function getMostCommonSpeciality(cluster) {
    return getMostCommon(cluster.map(x=>x.speciality))[0].keyWord;
}
function getMostCommonKeywords(cluster) {
    let result = cluster.map(post => post.keywords).reduce(flatten, []);
    return getMostCommon(result).filter((x, i, arr)=> arr.length/2 > i).map(x=>x.keyWord);
}
