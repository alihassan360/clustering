import posts from "./raw3.json";
import { extractKeywordsV2, flatten, aggregator, objectToArray, getMatchScore, detectLanguages, extendPostWithLanguage,extendPostWithKeyWords, exportJSON } from "./util";

console.log(posts.length);


// Print top words used for inspection

// const testPosts = posts.slice(0, 100);
// let result = testPosts
//   .map(post => extractKeywordsV2(post.title + " " + post.description))
//   .reduce(flatten, [])
//   .reduce(aggregator, {});
// result = objectToArray(result).sort((a, b) => b.count - a.count);
// console.log(result);


// print score of first one with all other posts

// const currentPost = posts[0];
// const result = posts.map(post=> {
//   if(post._id === currentPost._id){
//     return undefined;
//   }
//   const {common, score} = getMatchScore(currentPost, post);
//   return {post: post._id, score, common, category: post.category}
// }).sort((a, b) => b.score - a.score)

// console.log(currentPost.category);
// console.log(result);

// write files with languages 
console.log("extendedPosts");
const extendedPosts = Promise.all(posts.map(extendPostWithKeyWords).map(extendPostWithLanguage)).then(exportJSON("src/raw3.json"))
// detectLanguages(post.title + " " +  post.desciption);